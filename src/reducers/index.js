import { combineReducers } from 'redux';

const songsReducer = () => {
  return [
    { title: 'Enjoy the Silence', duration: '4:15' },
    { title: '1979', duration: '4:21' },
    { title: 'Bizzarre Love Triangle', duration: '3:44' },
    { title: 'Pop Kids', duration: '3:56' },
  ];
};

const selectedSongReducer = (selectedSong=null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }
  
  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});
